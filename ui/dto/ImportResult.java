package de.christophbrill.appframework.ui.dto;

public class ImportResult {

    public int skipped;
    public int created;
    public int updated;
    public int deleted;

}
