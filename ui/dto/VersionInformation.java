package de.christophbrill.appframework.ui.dto;

import java.time.LocalDateTime;

public class VersionInformation {

    public String maven;
    public String git;
    public LocalDateTime buildTimestamp;

    public VersionInformation() {}

    public VersionInformation(String maven, String git, LocalDateTime buildTimestamp) {
        this.maven = maven;
        this.git = git;
        this.buildTimestamp = buildTimestamp;
    }
}
