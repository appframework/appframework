package de.christophbrill.appframework.ui.dto;

public class DeletionAction {

    public String effect;
    public String action;
    public Integer affected;

    public DeletionAction() {}

    public DeletionAction(String effect, String action) {
        this.effect = effect;
        this.action = action;
    }
}
