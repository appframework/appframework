package de.christophbrill.appframework.ui.dto;

import java.util.List;

public class User extends AbstractDto {

    public String name;
    public String login;
    public String password;
    public String email;

    @Deprecated
    public List<Long> roleIds;

    public List<String> roles;
    public Long pictureId;
}
