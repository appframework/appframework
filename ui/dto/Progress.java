package de.christophbrill.appframework.ui.dto;

public class Progress<T> {

    public String key;
    public T result;
    public int value;
    public Integer max;
    public String message;
    public boolean completed;
    public boolean success;

}
