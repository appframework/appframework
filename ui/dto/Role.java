package de.christophbrill.appframework.ui.dto;

import java.util.ArrayList;
import java.util.List;

public class Role extends AbstractDto {

    public String name;
    public List<String> permissions = new ArrayList<>(0);
}
