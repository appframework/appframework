package de.christophbrill.appframework.ui.dto;

public class Problem {

    public String title;
    public int status;
    public String detail;

    public Problem(String title, int status, String detail) {
        this.title = title;
        this.status = status;
        this.detail = detail;
    }
}
