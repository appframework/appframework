package de.christophbrill.appframework.ui.async;

import de.christophbrill.appframework.ui.dto.Progress;
import de.christophbrill.appframework.ui.dto.ImportResult;
import jakarta.inject.Singleton;
import jakarta.websocket.Session;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/**
 * A central hub to store progress values in.
 */
@Singleton
public class ProgressCache {

    public static class CacheEntry {

        public CacheEntry(Progress<?> progress) {
            this.progress = progress;
        }

        public Progress<?> progress;
        public Session session;
    }

    private final Map<String, CacheEntry> cache = new HashMap<>();

    public Progress<ImportResult> createProgress() {
        Progress<ImportResult> progress = new Progress<>();
        String random = UUID.randomUUID().toString();
        progress.key = random;
        cache.put(random, new CacheEntry(progress));
        return progress;
    }

    public Set<Map.Entry<String, CacheEntry>> entries() {
        return cache.entrySet();
    }

    public void remove(String id) {
        cache.remove(id);
    }

    public CacheEntry get(String id) {
        return cache.get(id);
    }

}
