package de.christophbrill.appframework.ui.async;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.christophbrill.appframework.ui.dto.Progress;
import io.quarkus.logging.Log;
import io.quarkus.scheduler.Scheduled;
import jakarta.inject.Inject;
import jakarta.websocket.OnOpen;
import jakarta.websocket.Session;
import jakarta.websocket.server.PathParam;
import jakarta.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.nio.channels.ClosedChannelException;
import java.util.Map;

@ServerEndpoint("/async/progress/{progressId}")
public class ProgressEndpoint {

    @Inject
    ProgressCache cache;

    private final ObjectMapper mapper = new ObjectMapper();

    @Scheduled(every = "1s")
    public void sendStatus() {
        Log.trace("Checking if relevant transfers are in progress");
        for (Map.Entry<String, ProgressCache.CacheEntry> entry : cache.entries()) {
            String id = entry.getKey();
            Log.tracev("Progress exists for {0}", id);
            try {
                Progress<?> progress = entry.getValue().progress;
                Session session = entry.getValue().session;
                if (progress.completed) {
                    Log.tracev("Progress {0} is completed", id);
                    if (session != null) {
                        session.getBasicRemote().sendText(mapper.writeValueAsString(progress), true);
                        session.close();
                    }
                    cache.remove(id);
                } else {
                    if (session != null) {
                        Log.tracev("Sending update for progress {0}", id);
                        session.getBasicRemote().sendText(mapper.writeValueAsString(progress));
                    }
                }
            } catch (IOException e) {
                if (
                    e.getCause() == null ||
                    e.getCause().getCause() == null ||
                    !(e.getCause().getCause() instanceof ClosedChannelException)
                ) {
                    Log.error(e.getMessage(), e);
                }
                cache.remove(id);
            }
        }
    }

    @OnOpen
    public void registerProgressListener(@PathParam("progressId") final String id, final Session session) {
        ProgressCache.CacheEntry cacheEntry = cache.get(id);
        if (cacheEntry != null) {
            cacheEntry.session = session;
        }
    }
}
