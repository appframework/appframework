package de.christophbrill.appframework.ui.exceptions;

import de.christophbrill.appframework.ui.dto.Problem;
import jakarta.ws.rs.core.Response;
import java.io.Serial;

public class NotFoundException extends ProblemException {

    @Serial
    private static final long serialVersionUID = 4390417386853213093L;

    public NotFoundException(String message) {
        super(
            Response.Status.NOT_FOUND,
            new Problem("NotFoundException", Response.Status.NOT_FOUND.getStatusCode(), message)
        );
    }
}
