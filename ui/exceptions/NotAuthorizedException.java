package de.christophbrill.appframework.ui.exceptions;

import de.christophbrill.appframework.ui.dto.Problem;
import jakarta.ws.rs.core.Response;
import java.io.Serial;

public class NotAuthorizedException extends ProblemException {

    @Serial
    private static final long serialVersionUID = -2414323686774901121L;

    public NotAuthorizedException() {
        super(
            Response.Status.UNAUTHORIZED,
            new Problem(
                "NotAuthorizedException",
                Response.Status.UNAUTHORIZED.getStatusCode(),
                null
            )
        );
    }

    public NotAuthorizedException(String message) {
        super(
            Response.Status.UNAUTHORIZED,
            new Problem(
                "NotAuthorizedException",
                Response.Status.UNAUTHORIZED.getStatusCode(),
                message
            )
        );
    }
}
