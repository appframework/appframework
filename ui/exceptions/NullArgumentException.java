package de.christophbrill.appframework.ui.exceptions;

import java.io.Serial;

public class NullArgumentException extends BadArgumentException {

    @Serial
    private static final long serialVersionUID = 7812375776293537525L;

    public NullArgumentException(String parameter) {
        super(parameter + " must not be null");
    }
}
