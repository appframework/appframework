package de.christophbrill.appframework.ui.exceptions;

import de.christophbrill.appframework.ui.dto.Problem;
import jakarta.ws.rs.core.Response;
import java.io.Serial;

public class BadStateException extends ProblemException {

    @Serial
    private static final long serialVersionUID = -6356020778089345408L;

    public BadStateException(String message) {
        super(
            Response.Status.INTERNAL_SERVER_ERROR,
            new Problem(
                "BadStateException",
                Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(),
                message
            )
        );
    }
}
