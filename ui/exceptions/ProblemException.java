package de.christophbrill.appframework.ui.exceptions;

import de.christophbrill.appframework.ui.dto.Problem;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;

public class ProblemException extends WebApplicationException {

    private static final long serialVersionUID = 6345134645677275799L;

    protected ProblemException(Response.Status status, Problem problem) {
        super(Response.status(status).entity(problem).build());
    }
}
