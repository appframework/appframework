package de.christophbrill.appframework.ui.exceptions;

import de.christophbrill.appframework.ui.dto.Problem;
import jakarta.ws.rs.core.Response;
import java.io.Serial;

public class BadArgumentException extends ProblemException {

    @Serial
    private static final long serialVersionUID = -115985314007602297L;

    public BadArgumentException(String message) {
        super(
            Response.Status.BAD_REQUEST,
            new Problem(
                "BadArgumentException",
                Response.Status.BAD_REQUEST.getStatusCode(),
                message
            )
        );
    }
}
