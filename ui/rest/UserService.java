package de.christophbrill.appframework.ui.rest;

import de.christophbrill.appframework.mapping.UserMapper;
import de.christophbrill.appframework.persistence.model.UserEntity;
import de.christophbrill.appframework.persistence.selector.UserSelector;
import de.christophbrill.appframework.ui.dto.User;
import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.appframework.util.PasswordHasher;
import jakarta.annotation.Nonnull;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.Path;

@Path("/user")
public class UserService extends AbstractResourceService<User, UserEntity> {

    protected Class<UserEntity> getEntityClass() {
        return UserEntity.class;
    }

    protected UserSelector getSelector() {
        return new UserSelector(em);
    }

    @Override
    protected UserMapper getMapper() {
        return UserMapper.INSTANCE;
    }

    protected String getCreatePermission() {
        return "ADMIN_USERS";
    }

    protected String getReadPermission() {
        return "SHOW_USERS";
    }

    protected String getUpdatePermission() {
        return "ADMIN_USERS";
    }

    protected String getDeletePermission() {
        return "ADMIN_USERS";
    }

    @Override
    @Transactional
    public User create(User dto) {
        PasswordHasher.hashPassword(dto);
        return super.create(dto);
    }

    @Override
    @Transactional
    public void update(@Nonnull Long id, User dto) {
        PasswordHasher.hashPassword(dto);
        super.update(id, dto);
    }

    @Override
    protected void validate(User dto, UserEntity entity) {
        var user = UserEntity.<UserEntity>find("login", entity.login).firstResult();
        if (user != null && !entity.id.equals(user.id)) {
            throw new BadArgumentException("Login already taken by another user");
        }
    }
}
