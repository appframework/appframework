package de.christophbrill.appframework.ui.rest;

import io.quarkus.security.identity.SecurityIdentity;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;

public abstract class AbstractService {

    @Inject
    protected EntityManager em;

    @Inject
    protected SecurityIdentity identity;
}
