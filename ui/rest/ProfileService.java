package de.christophbrill.appframework.ui.rest;

import de.christophbrill.appframework.mapping.UserMapper;
import de.christophbrill.appframework.persistence.model.BinaryDataEntity;
import de.christophbrill.appframework.persistence.model.UserEntity;
import de.christophbrill.appframework.ui.dto.User;
import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.appframework.util.PasswordHasher;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import java.io.IOException;
import java.net.URLConnection;
import java.nio.file.Files;
import org.jboss.resteasy.reactive.RestForm;
import org.jboss.resteasy.reactive.multipart.FileUpload;

@Path("/profile")
public class ProfileService extends AbstractService {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public User get() {
        UserEntity user = UserEntity.<UserEntity>find("login", identity.getPrincipal().toString()).firstResult();
        return UserMapper.INSTANCE.mapEntityToDto(user);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    public User update(User user) {
        var existing_ = UserEntity.<UserEntity>find("login", identity.getPrincipal().toString()).firstResult();
        var existing = validate(existing_.id, user.login);
        // Only update some details
        PasswordHasher.hashPassword(user);

        existing.name = user.name;
        existing.password = user.password;
        existing.email = user.email;
        existing.login = user.login;
        existing.persist();

        return UserMapper.INSTANCE.mapEntityToDto(existing);
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("/upload")
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Long uploadFile(@RestForm String filename,
                           @RestForm("file") FileUpload file) {
        var user = UserEntity.<UserEntity>find("login", identity.getPrincipal().toString()).firstResult();
        BinaryDataEntity binaryData = new BinaryDataEntity();
        binaryData.filename = filename;
        byte[] bytes;
        try {
            bytes = Files.readAllBytes(file.uploadedFile());
        } catch (IOException e) {
            throw new BadArgumentException("Could not read image: " + e.getMessage());
        }
        binaryData.size = bytes.length;
        binaryData.data = bytes;
        binaryData.contentType = URLConnection.guessContentTypeFromName(binaryData.filename);
        user.picture = binaryData;
        binaryData.persist();
        user.persist();
        return binaryData.id;
    }

    private UserEntity validate(Long id, String login) {
        var user = UserEntity.<UserEntity>find("login", login).firstResult();
        if (!id.equals(user.id)) {
            throw new BadArgumentException("Login already taken by another user");
        }
        return user;
    }
}
