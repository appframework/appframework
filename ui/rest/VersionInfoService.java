package de.christophbrill.appframework.ui.rest;

import de.christophbrill.appframework.ui.dto.VersionInformation;
import de.christophbrill.appframework.util.VersionExtractor;
import jakarta.annotation.security.PermitAll;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.config.inject.ConfigProperty;

@Path("/version_info")
@Produces(MediaType.APPLICATION_JSON)
public class VersionInfoService {

    @ConfigProperty(name = "quarkus.application.version")
    String version;

    @ConfigProperty(name = "quarkus.application.name")
    String artifactId;

    @GET
    @PermitAll
    public VersionInformation getVersion() {
        return new VersionInformation(
            version,
            VersionExtractor.getGitVersion("de.christophbrill.appframework", artifactId),
            VersionExtractor.getBuildTimestamp("de.christophbrill.appframework", artifactId)
        );
    }
}
