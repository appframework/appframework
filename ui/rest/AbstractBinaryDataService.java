package de.christophbrill.appframework.ui.rest;

import de.christophbrill.appframework.persistence.model.BinaryDataEntity;
import de.christophbrill.appframework.persistence.model.UserEntity;
import jakarta.annotation.Nonnull;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.StreamingOutput;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Date;

public abstract class AbstractBinaryDataService extends AbstractService {

    private static final String RFC1123_DATE_FORMAT_PATTERN = "EEE, dd MMM yyyy HH:mm:ss zzz";

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getRaw(@PathParam("id") Long id) {
        var binaryData = BinaryDataEntity.<BinaryDataEntity>findById(id);
        if (binaryData == null) {
            throw new NotFoundException("No binary data with given ID");
        }

        if (binaryData.data == null) {
            throw new BadRequestException("Binary data is empty");
        }

        return Response.ok((StreamingOutput) outputStream -> outputStream.write(binaryData.data))
            .header(
                "Content-Disposition",
                "attachment; filename=\"" +
                binaryData.filename +
                "\"; size=" +
                binaryData.size +
                "; creation-date=\"" +
                new SimpleDateFormat(RFC1123_DATE_FORMAT_PATTERN).format(
                    Date.from(binaryData.created.atZone(ZoneId.systemDefault()).toInstant())
                ) +
                "\""
            )
            .header("Content-Length", binaryData.size)
            .build();
    }

    BufferedImage resizeImage(BufferedImage originalImage, int targetWidth, int targetHeight) {
        BufferedImage resizedImage = new BufferedImage(targetWidth, targetHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics2D = resizedImage.createGraphics();
        graphics2D.drawImage(originalImage, 0, 0, targetWidth, targetHeight, null);
        graphics2D.dispose();
        return resizedImage;
    }

    @GET
    @Path("/preview/{id}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getPreview(@PathParam("id") Long id) {
        var binaryData = BinaryDataEntity.<BinaryDataEntity>findById(id);
        if (binaryData == null) {
            throw new NotFoundException("No binary data with given ID");
        }

        if (binaryData.data == null) {
            throw new BadRequestException("Binary data is empty");
        }

        if (!binaryData.contentType.startsWith("image/")) {
            throw new BadRequestException("Only images supported");
        }

        try {
            BufferedImage read = ImageIO.read(new ByteArrayInputStream(binaryData.data));

            float aspectRatio = (float) read.getHeight() / read.getWidth();
            BufferedImage bufferedImage = resizeImage(read, 500, (int) (500 * aspectRatio));

            return Response.ok((StreamingOutput) outputStream -> ImageIO.write(bufferedImage, "jpg", outputStream))
                    .build();

        } catch (IOException e) {
            throw new BadRequestException("Failed to read image " + e.getMessage());
        }


    }

    @DELETE
    @Path("/{id}")
    @Transactional
    public void delete(@PathParam("id") Long id) {
        nullAndPersistOwnUsers(id);
        nullAndPersistUsers(id);
        BinaryDataEntity.deleteById(id);
    }

    private void nullAndPersistOwnUsers(@Nonnull Long id) {
        var users = UserEntity.<UserEntity>find("picture.id", id).list();
        for (var user : users) {
            user.picture = null;
            user.persist();
        }
    }

    /**
     * The implementation of this method is responsible to detach any owner from the binary data object given by the ID.
     *
     * @param id the ID of the binary data object to be detached
     */
    protected abstract void nullAndPersistUsers(@Nonnull Long id);
}
