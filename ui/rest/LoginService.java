package de.christophbrill.appframework.ui.rest;

import de.christophbrill.appframework.persistence.model.RoleEntity;
import de.christophbrill.appframework.persistence.model.UserEntity;
import de.christophbrill.appframework.ui.dto.Credentials;
import jakarta.annotation.security.PermitAll;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

@Path("/login")
public class LoginService {

    @PermitAll
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response login(Credentials credentials) {
        UserEntity userEntity = UserEntity.<UserEntity>find("login", credentials.username).firstResult();
        if (userEntity != null && userEntity.password.equals(credentials.password)) {
            RoleEntity r = RoleEntity.findByUserLogin(userEntity.login).firstResult();
            return Response.ok().entity(r.permissions).build();
        }
        Map<String, String> result = new HashMap<>();
        result.put("message", "invalid_username_or_password");
        return Response.status(Response.Status.UNAUTHORIZED).entity(result).build();
    }

    @GET
    @Path("/logout")
    @Produces(MediaType.APPLICATION_JSON)
    public Response logout() {
        return Response.status(Response.Status.OK).build();
    }
}
