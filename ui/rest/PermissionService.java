package de.christophbrill.appframework.ui.rest;

import de.christophbrill.appframework.persistence.model.RoleEntity;
import de.christophbrill.appframework.util.Permissions;
import io.quarkus.security.identity.SecurityIdentity;
import jakarta.annotation.security.PermitAll;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Path("/permissions")
public class PermissionService {

    @Inject
    SecurityIdentity identity;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<String> getPermissions() {
        return new ArrayList<>(Permissions.PERMISSIONS);
    }

    @GET
    @Path("/my")
    @PermitAll
    @Produces(MediaType.APPLICATION_JSON)
    public List<String> getMyPermissions() {
        var roles = RoleEntity.findByUserLogin(identity.getPrincipal().getName()).list();
        return roles.stream().flatMap((RoleEntity r) -> r.permissions.stream()).distinct().collect(Collectors.toList());
    }
}
