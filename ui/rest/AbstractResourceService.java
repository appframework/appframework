package de.christophbrill.appframework.ui.rest;

import de.christophbrill.appframework.mapping.ResourceMapper;
import de.christophbrill.appframework.persistence.deletion.CascadeDeletion;
import de.christophbrill.appframework.persistence.model.DbObject;
import de.christophbrill.appframework.persistence.selector.AbstractResourceSelector;
import de.christophbrill.appframework.ui.dto.AbstractDto;
import de.christophbrill.appframework.ui.dto.DeletionAction;
import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import de.christophbrill.appframework.ui.exceptions.BadStateException;
import de.christophbrill.appframework.ui.exceptions.NotAuthorizedException;
import de.christophbrill.appframework.ui.exceptions.NotFoundException;
import de.christophbrill.appframework.ui.exceptions.NullArgumentException;
import de.christophbrill.appframework.util.CascadeDeletionRegistry;
import io.quarkus.logging.Log;
import jakarta.annotation.Nonnull;
import jakarta.inject.Inject;
import jakarta.persistence.RollbackException;
import jakarta.transaction.Transactional;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.hibernate.exception.ConstraintViolationException;
import org.jboss.resteasy.reactive.RestResponse;

public abstract class AbstractResourceService<D extends AbstractDto, E extends DbObject>
    extends AbstractService {

    protected abstract Class<E> getEntityClass();

    protected abstract AbstractResourceSelector<E> getSelector();

    protected abstract ResourceMapper<D, E> getMapper();

    protected abstract String getCreatePermission();

    protected abstract String getReadPermission();

    protected abstract String getUpdatePermission();

    protected abstract String getDeletePermission();

    protected void checkReadPermission() {
        String permission = getReadPermission();
        if (!identity.hasRole(permission)) {
            throw new NotAuthorizedException("Missing " + permission);
        }
    }

    @Inject
    Validator validator;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public RestResponse<List<D>> getByIds(
        @QueryParam("ids") List<Long> ids,
        @QueryParam("offset") Integer offset,
        @QueryParam("limit") Integer limit,
        @QueryParam("sortColumn") String sortColumn,
        @QueryParam("ascending") Boolean ascending,
        @QueryParam("search") String search
    ) {
        checkReadPermission();
        AbstractResourceSelector<E> selector = (AbstractResourceSelector<E>) getSelector()
            .withIds(ids)
            .withSearch(search)
            .withSortColumn(sortColumn, Boolean.TRUE.equals(ascending))
            .withOffset(offset)
            .withLimit(limit);

        List<E> entities = selector.findAll();

        var builder = RestResponse.ResponseBuilder
                .ok(getMapper().mapEntitiesToDtos(entities));

        if (offset != null || limit != null) {
            var count = selector.count();
            builder.header("Result-Count", Long.toString(count));
        }

        return builder.build();
    }

    protected void checkCreatePermission() {
        String permission = getCreatePermission();
        if (!identity.hasRole(permission)) {
            throw new NotAuthorizedException("Missing " + permission);
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public D create(D dto) {
        checkCreatePermission();
        if (dto == null) {
            throw new NullArgumentException("t");
        }
        if (dto.id != null) {
            throw new BadArgumentException("Cannot create an entity already having an ID");
        }
        var entity = mapCreate(dto);
        try {
            validate(dto, entity);
            entity.persist();
            return getMapper().mapEntityToDto(entity);
        } catch (ConstraintViolationException e) {
            throw new BadArgumentException(e.getMessage());
        }
    }

    protected E mapCreate(D dto) {
        return getMapper().mapDtoToEntity(dto);
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public D getById(@PathParam("id") Long id) {
        checkReadPermission();
        if (id == null) {
            throw new NullArgumentException("id");
        }

        var entity = findById(id);
        return getMapper().mapEntityToDto(entity);
    }

    protected void checkUpdatePermission() {
        String permission = getUpdatePermission();
        if (!identity.hasRole(permission)) {
            throw new NotAuthorizedException("Missing " + permission);
        }
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public void update(@PathParam("id") @Nonnull Long id, D dto) {
        checkUpdatePermission();
        if (dto == null) {
            throw new NullArgumentException("t");
        }
        if (dto.id == null) {
            throw new BadArgumentException("Cannot update an entity without an ID");
        }
        var entity = mapUpdate(dto);
        if (entity == null) {
            throw new NotFoundException("Could not find t with ID " + id);
        }
        try {
            validate(dto, entity);
            entity.persist();
        } catch (ConstraintViolationException e) {
            throw new BadArgumentException(e.getMessage());
        }
    }

    protected E mapUpdate(D dto) {
        return getMapper().mapDtoToEntity(dto);
    }

    protected void checkDeletePermission() {
        String permission = getDeletePermission();
        if (!identity.hasRole(permission)) {
            throw new NotAuthorizedException("Missing " + permission);
        }
    }

    protected E findById(Long id) {
        return em.find(getEntityClass(), id);
    }

    @GET
    @Path("/deletable/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<DeletionAction> checkDelete(@PathParam("id") @Nonnull Long id) {
        checkDeletePermission();
        List<CascadeDeletion<E>> cascadeDeletions = CascadeDeletionRegistry.getCascadeDeletion(getEntityClass());
        var entity = findById(id);
        List<DeletionAction> result = new ArrayList<>();
        for (CascadeDeletion<E> cascadeDeletion : cascadeDeletions) {
            var i = cascadeDeletion.affectedByDeletion(em, entity);
            if (i > 0) {
                DeletionAction action = cascadeDeletion.getAction();
                action.affected = i;
                result.add(action);
            }
        }
        return result;
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    public void delete(@PathParam("id") @Nonnull Long id) {
        checkDeletePermission();
        try {
            var entity = findById(id);
            if (entity == null) {
                throw new BadArgumentException("Entity with ID " + id + " does not exist");
            }

            // Perform cascade deletion actions
            List<CascadeDeletion<E>> cascadeDeletions = CascadeDeletionRegistry.getCascadeDeletion(
                getEntityClass()
            );
            for (CascadeDeletion<E> cascadeDeletion : cascadeDeletions) {
                cascadeDeletion.delete(em, entity);
            }
            entity.delete();
        } catch (RollbackException e) {
            Log.error(e.getMessage());
            throw new BadStateException(e.getMessage() + ", see server log for details");
        }
    }

    protected void validate(D dto, E entity) {
        Set<ConstraintViolation<E>> vioalations = validator.validate(entity);
        if (!vioalations.isEmpty()) {
            String message = vioalations
                .stream()
                .map(violation -> violation.getPropertyPath() + " " + violation.getMessage())
                .collect(Collectors.joining(", "));
            throw new BadArgumentException(message);
        }
    }
}
