package de.christophbrill.appframework.ui.rest;

import de.christophbrill.appframework.ui.async.ProgressCache;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

@Path("/progress")
public class ProgressService extends AbstractService {

    @Inject
    ProgressCache cache;

    @Path("/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProgress(@PathParam("id") String id) {
        ProgressCache.CacheEntry progress = cache.get(id);
        if (progress == null) {
            return Response.status(Status.NOT_FOUND).build();
        }
        return Response.ok().entity(progress.progress).build();
    }
}
