package de.christophbrill.appframework.ui.rest;

import de.christophbrill.appframework.mapping.RoleMapper;
import de.christophbrill.appframework.persistence.model.RoleEntity;
import de.christophbrill.appframework.persistence.selector.RoleSelector;
import de.christophbrill.appframework.ui.dto.Role;
import jakarta.ws.rs.Path;

@Path("/role")
public class RoleService extends AbstractResourceService<Role, RoleEntity> {

    protected Class<RoleEntity> getEntityClass() {
        return RoleEntity.class;
    }

    protected RoleSelector getSelector() {
        return new RoleSelector(em);
    }

    @Override
    protected RoleMapper getMapper() {
        return RoleMapper.INSTANCE;
    }

    protected String getCreatePermission() {
        return "ADMIN_ROLES";
    }

    protected String getReadPermission() {
        return "SHOW_ROLES";
    }

    protected String getUpdatePermission() {
        return "ADMIN_ROLES";
    }

    protected String getDeletePermission() {
        return "ADMIN_ROLES";
    }
}
