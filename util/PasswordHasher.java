package de.christophbrill.appframework.util;

import de.christophbrill.appframework.persistence.model.UserEntity;
import de.christophbrill.appframework.ui.dto.User;
import jakarta.annotation.Nullable;
import org.apache.commons.lang3.StringUtils;

public class PasswordHasher {

    public static void hashPassword(@Nullable User user) {
        if (user != null && StringUtils.isNotEmpty(user.password)) {
            //Hashed on client side
        } else if (user != null && user.id != null) {
            user.password = UserEntity.<UserEntity>findById(user.id).getPassword();
        }
    }

    public static void hashPassword(@Nullable UserEntity user) {
        if (user != null && StringUtils.isNotEmpty(user.getPassword())) {
            //Hashed on client side
        } else if (user != null && user.id != null) {
            user.password = UserEntity.<UserEntity>findById(user.id).getPassword();
        }
    }
}
