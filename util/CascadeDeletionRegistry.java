package de.christophbrill.appframework.util;

import de.christophbrill.appframework.persistence.deletion.CascadeDeletion;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CascadeDeletionRegistry {

    private static final Map<Class<?>, List<CascadeDeletion<?>>> registry = new HashMap<>();

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static <T> List<CascadeDeletion<T>> getCascadeDeletion(Class<T> entityClass) {
        List<CascadeDeletion<?>> cascadeDeletions = registry.get(entityClass);
        if (cascadeDeletions == null) {
            return Collections.emptyList();
        }
        return (List) cascadeDeletions;
    }

    public static <T> void addCascadeDeletion(Class<T> entityClass, CascadeDeletion<T> cascadeDeletion) {
        List<CascadeDeletion<?>> cascadeDeletions = registry.computeIfAbsent(entityClass, k -> new ArrayList<>());
        for (CascadeDeletion<?> existing : cascadeDeletions) {
            if (existing.getClass().equals(cascadeDeletion.getClass())) {
                return;
            }
        }
        cascadeDeletions.add(cascadeDeletion);
    }
}
