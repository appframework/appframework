package de.christophbrill.appframework.util;

import de.christophbrill.appframework.ui.dto.Problem;
import de.christophbrill.appframework.ui.exceptions.ProblemException;
import io.quarkus.logging.Log;
import jakarta.annotation.Nonnull;
import jakarta.annotation.Priority;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.Provider;

@Provider
@Priority(Priorities.USER)
public class LoggingExceptionMapper implements jakarta.ws.rs.ext.ExceptionMapper<Throwable> {

    @Nonnull
    @Override
    public Response toResponse(@Nonnull Throwable exception) {
        Log.error(exception.getMessage(), exception);
        if (exception instanceof ProblemException problemException) {
            return problemException.getResponse();
        }
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
            .entity(
                new Problem(
                    Response.Status.INTERNAL_SERVER_ERROR.getReasonPhrase(),
                    Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(),
                    exception.getMessage()
                )
            )
            .build();
    }

}
