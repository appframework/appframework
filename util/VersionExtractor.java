package de.christophbrill.appframework.util;

import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Properties;
import org.apache.commons.lang3.StringUtils;

public class VersionExtractor {

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ");

    @Nullable
    private static String load(
        @Nonnull String groupId,
        @Nonnull String artifactId,
        @Nonnull String file,
        @Nonnull String property
    ) {
        Properties properties = new Properties();
        try (
            InputStream pomPropertiesStream =
                VersionExtractor.class.getResourceAsStream("/META-INF/maven/" + groupId + "/" + artifactId + "/" + file)
        ) {
            if (pomPropertiesStream != null) {
                properties.load(pomPropertiesStream);
            }
        } catch (IOException | NullPointerException e) {
            return null;
        }
        String version = properties.getProperty(property);
        if (StringUtils.isEmpty(version)) {
            return null;
        }
        return version;
    }

    @Nonnull
    public static String getGitVersion(@Nonnull String groupId, @Nonnull String artifactId) {
        String version = load(groupId, artifactId, "git.properties", "git.commit.id.abbrev");
        if (StringUtils.isEmpty(version)) {
            version = "";
        }
        return version;
    }

    @Nullable
    public static LocalDateTime getBuildTimestamp(@Nonnull String groupId, @Nonnull String artifactId) {
        return toDate(load(groupId, artifactId, "git.properties", "git.build.time"));
    }

    @Nullable
    private static LocalDateTime toDate(@Nullable String string) {
        if (string == null) {
            return null;
        }
        try {
            return LocalDateTime.parse(string, FORMATTER);
        } catch (DateTimeParseException e) {
            return null;
        }
    }
}
