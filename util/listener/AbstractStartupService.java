package de.christophbrill.appframework.util.listener;

import de.christophbrill.appframework.persistence.deletion.RoleUserCascadeDeletion;
import de.christophbrill.appframework.persistence.deletion.UserBinaryDataCascadeDeletion;
import de.christophbrill.appframework.persistence.deletion.UserRoleCascadeDeletion;
import de.christophbrill.appframework.persistence.deletion.UserUserDeletion;
import de.christophbrill.appframework.persistence.model.RoleEntity;
import de.christophbrill.appframework.persistence.model.UserEntity;
import de.christophbrill.appframework.util.CascadeDeletionRegistry;
import de.christophbrill.appframework.util.Permissions;
import io.quarkus.runtime.StartupEvent;
import jakarta.enterprise.event.Observes;

public abstract class AbstractStartupService {

    public void contextInitialized(@Observes StartupEvent ev) {
        for (Enum<?> permission : getPermissions()) {
            Permissions.PERMISSIONS.add(permission.name());
        }

        CascadeDeletionRegistry.addCascadeDeletion(UserEntity.class, new UserRoleCascadeDeletion());
        CascadeDeletionRegistry.addCascadeDeletion(UserEntity.class, new UserBinaryDataCascadeDeletion());
        CascadeDeletionRegistry.addCascadeDeletion(UserEntity.class, new UserUserDeletion());
        CascadeDeletionRegistry.addCascadeDeletion(RoleEntity.class, new RoleUserCascadeDeletion());
        initCascadeDeletions();
    }

    protected abstract Enum<?>[] getPermissions();

    protected abstract void initCascadeDeletions();
}
