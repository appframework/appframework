package de.christophbrill.appframework.mapping;

import de.christophbrill.appframework.persistence.model.RoleEntity;
import de.christophbrill.appframework.ui.dto.Role;
import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import org.mapstruct.ObjectFactory;

public class RoleFactory {

    @ObjectFactory
    public RoleEntity createEntity(Role dto) {
        if (dto != null && dto.id != null) {
            RoleEntity roleEntity = RoleEntity.findById(dto.id);
            if (roleEntity == null) {
                throw new BadArgumentException("Role with ID " + dto.id + " not found");
            }
            return roleEntity;
        }
        return new RoleEntity();
    }
}
