package de.christophbrill.appframework.mapping;

import de.christophbrill.appframework.persistence.model.BinaryDataEntity;
import de.christophbrill.appframework.ui.dto.BinaryData;
import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import org.mapstruct.ObjectFactory;

public class BinaryDataFactory {

    @ObjectFactory
    public BinaryDataEntity createEntity(BinaryData dto) {
        if (dto != null && dto.id != null) {
            BinaryDataEntity binaryDataEntity = BinaryDataEntity.findById(dto.id);
            if (binaryDataEntity == null) {
                throw new BadArgumentException("BinaryData with ID " + dto.id + " not found");
            }
            return binaryDataEntity;
        }
        return new BinaryDataEntity();
    }
}
