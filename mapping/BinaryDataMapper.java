package de.christophbrill.appframework.mapping;

import de.christophbrill.appframework.persistence.model.BinaryDataEntity;
import de.christophbrill.appframework.ui.dto.BinaryData;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = BinaryDataFactory.class)
public interface BinaryDataMapper extends ResourceMapper<BinaryData, BinaryDataEntity> {
    BinaryDataMapper INSTANCE = Mappers.getMapper(BinaryDataMapper.class);

    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "data", ignore = true)
    @Override
    BinaryDataEntity mapDtoToEntity(BinaryData binaryData);
}
