package de.christophbrill.appframework.mapping;

import de.christophbrill.appframework.persistence.model.RoleEntity;
import de.christophbrill.appframework.ui.dto.Role;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = RoleFactory.class)
public interface RoleMapper extends ResourceMapper<Role, RoleEntity> {
    RoleMapper INSTANCE = Mappers.getMapper(RoleMapper.class);

    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "users", ignore = true)
    @Override
    RoleEntity mapDtoToEntity(Role role);
}
