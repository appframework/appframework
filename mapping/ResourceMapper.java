package de.christophbrill.appframework.mapping;

import java.util.List;
import org.mapstruct.MappingTarget;

public interface ResourceMapper<DTO, ENTITY> {
    ENTITY mapDtoToEntity(DTO dto);

    DTO mapEntityToDto(ENTITY entity);

    List<ENTITY> mapDtosToEntities(List<DTO> dtos);

    void mapDtosToEntities(List<DTO> dtos, @MappingTarget List<ENTITY> entities);

    List<DTO> mapEntitiesToDtos(List<ENTITY> entities);
}
