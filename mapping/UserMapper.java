package de.christophbrill.appframework.mapping;

import de.christophbrill.appframework.persistence.model.BinaryDataEntity;
import de.christophbrill.appframework.persistence.model.RoleEntity;
import de.christophbrill.appframework.persistence.model.UserEntity;
import de.christophbrill.appframework.ui.dto.User;
import java.util.ArrayList;
import org.apache.commons.collections4.CollectionUtils;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

@Mapper(uses = UserFactory.class)
public interface UserMapper extends ResourceMapper<User, UserEntity> {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    @Mapping(target = "roles", ignore = true)
    @Mapping(target = "roleIds", ignore = true)
    @Mapping(target = "pictureId", ignore = true)
    @Mapping(target = "password", ignore = true)
    @Override
    User mapEntityToDto(UserEntity entity);

    @AfterMapping
    default void customDtoToEntity(User dto, @MappingTarget UserEntity entity) {
        if (CollectionUtils.isNotEmpty(dto.roles)) {
            entity.roles = RoleEntity.<RoleEntity>find("name in ?1", dto.roles).list();
        } else if (CollectionUtils.isNotEmpty(dto.roleIds)) {
            entity.roles = RoleEntity.<RoleEntity>find("id in ?1", dto.roleIds).list();
        } else {
            entity.roles = new ArrayList<>(0);
        }
        if (dto.pictureId != null) {
            entity.picture = BinaryDataEntity.findById(dto.pictureId);
        } else {
            entity.picture = null;
        }
    }

    @Mapping(target = "created", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "roles", ignore = true)
    @Mapping(target = "picture", ignore = true)
    @Override
    UserEntity mapDtoToEntity(User user);

    @AfterMapping
    default void customEntityToDto(UserEntity entity, @MappingTarget User dto) {
        dto.password = null;
        dto.roleIds = entity.roles.stream().map(r -> r.id).toList();
        dto.roles = entity.roles.stream().map(r -> r.name).toList();
        dto.pictureId = entity.picture != null ? entity.picture.id : null;
    }
}
