package de.christophbrill.appframework.mapping;

import de.christophbrill.appframework.persistence.model.UserEntity;
import de.christophbrill.appframework.ui.dto.User;
import de.christophbrill.appframework.ui.exceptions.BadArgumentException;
import org.mapstruct.ObjectFactory;

public class UserFactory {

    @ObjectFactory
    public UserEntity createEntity(User dto) {
        if (dto != null && dto.id != null) {
            UserEntity userEntity = UserEntity.findById(dto.id);
            if (userEntity == null) {
                throw new BadArgumentException("User with ID " + dto.id + " not found");
            }
            return userEntity;
        }
        return new UserEntity();
    }
}
