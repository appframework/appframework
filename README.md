# appframework

The "appframework" projects aims to aid writing small applications or
prototypes that follow a standard scheme. This implies:

The backend itself is written in quarkus (3.5 at the time of writing). It makes
used of *postgresql* with *flyway* and offers a standard *CRUD* API for
accessing the data.

Tests for the API as well as layers below are hosted in https://gitlab.com/appframework/appframework-testing.
Using these tests is optional but strongly recommend.

The frontend is based on a single page application written in AngularJS 1.x and
hosted at https://gitlab.com/appframework/appframework-angular. Using the
frontend is optional.

## Usage

The project itself is currently to be used as git submodule, to remove the
complexity of releases. To add the appframework to your project execute the
following command:

    git submodule add git@gitlab.com:appframework/appframework.git src/main/java/de/christophbrill/appframework

An example using the submodule is to be found at https://gitlab.com/appframework/tasker-web

## Core features

### CRUD API

To implement services you have to implement *de.christophbrill.appframework.ui.rest.AbstractResourceService*.
Implementing the methods implies also providing a data transfer object (DTO)
which is exposed in the REST API. It can differ from the entity stored in the
database which you will also need.

An example entity looks like this:

```java
@Entity(name = "Label")
@Table(name = "label")
public class LabelEntity extends DbObject {
    public String name;
    @ManyToOne
    public ColorEntity color;
}
```

The matching DTO will look like this:
```java
public class Label extends AbstractDto {
    public String name;
    public Long colorId;
}
```

To map between the entities *mapstruct* is used, so you will also need the
following mapper and object factory:

```java
@Mapper(uses = {LabelFactory.class})
public interface LabelMapper extends ResourceMapper<Label, LabelEntity> {

    LabelMapper INSTANCE = Mappers.getMapper(LabelMapper.class);

    @Mapping(target = "modifiedBy", ignore = true)
    @Mapping(target = "modified", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "created", ignore = true)
    @Mapping(target = "colorId", ignore = true)
    @Override
    LabelEntity mapDtoToEntity(Label dto);

    @Mapping(target = "color", ignore = true)
    @Override
    Label mapEntityToDto(LabelEntity entity);

    @AfterMapping
    default void customDtoToEntityBase(Label dto, @MappingTarget LabelEntity entity) {
        if (dto.colorId != null) {
            entity.color = ColorEntity.findById(dto.colorId);
        } else {
            entity.color = null;
        }
    }

    @AfterMapping
    default void customEntityToDtoBase(LabelEntity entity, @MappingTarget Label dto) {
        if (entity.color != null) {
            dto.colorId = entity.color.id;
        } else {
            dto.colorId = null;
        }
    }

}
```

```java
public class LabelFactory {
    @ObjectFactory
    public LabelEntity createEntity(Label dto) {
        if (dto != null && dto.id != null) {
            LabelEntity entity = LabelEntity.findById(dto.id);
            if (entity == null) {
                throw new BadArgumentException("Label with ID " + dto.id + " not found");
            }
            return entity;
        }
        return new LabelEntity();
    }
}
```

The last step is to implement a selector, which is used to perform search
queries on the existing data.

```java
public class LabelSelector extends AbstractResourceSelector<LabelEntity> {

    private String search;

    public LabelSelector(EntityManager em) {
        super(em);
    }

    @Override
    @Nonnull
    protected List<Predicate> generatePredicateList(@Nonnull CriteriaBuilder builder,
                                                    @Nonnull Root<LabelEntity> from,
                                                    @Nonnull CriteriaQuery<?> criteriaQuery) {
        List<Predicate> predicates = super.generatePredicateList(builder, from, criteriaQuery);

        if (StringUtils.isNotEmpty(search)) {
            predicates.add(builder.like(builder.lower(from.get(LabelEntity_.name)), '%' + search.toLowerCase() + '%'));
        }

        return predicates;
    }

    @Override
    public LabelSelector withSearch(String search) {
        this.search = search;
        return this;
    }

    @Override
    @Nonnull
    protected Class<LabelEntity> getEntityClass() {
        return LabelEntity.class;
    }

}
```

Following this pattern you can provide a simple REST CRUD API to your frontend:

```java
@Path("/label")
public class LabelService extends AbstractResourceService<Label, LabelEntity> {

    protected Class<LabelEntity> getEntityClass() {
        return LabelEntity.class;
    }
    protected LabelSelector getSelector() {
        return new LabelSelector(em);
    }
    @Override
    protected LabelMapper getMapper() {
        return LabelMapper.INSTANCE;
    }
    protected String getCreatePermission() {
        return "ADMIN_LABELS";
    }
    protected String getReadPermission() {
        return "SHOW_LABELS";
    }
    protected String getUpdatePermission() {
        return "ADMIN_LABELS";
    }
    protected String getDeletePermission() {
        return "ADMIN_LABELS";
    }

}
```

## Users, roles, permission

The task of adding authentication to your application is already handled in the
*User* and *Role* classes. The roles themselved don't define permissions, but
an additional layer is being used to allow aggregating permissions into roles.
Therefore the logic is:

- Each user has one or more roles
- Each role has one or more permissions
- During login the permissions are loaded into the session and can be used
  to protect API calls
- The common differentiation is "ADMIN_XXX" for modifying calls, and "SHOW_" for
  read-only calls.

## Cascade deletions

When removig data where you don't want the database to implicitly remove
associated data, you can register cascade deletion handlers. To do so you need
to add to in your StartupListener

```java
CascadeDeletionRegistry.addCascadeDeletion(LabelEntity.class, new LabelTaskCascadeDeletion());
```

A cascade deletion handle is queried before deletion, the check if data will be
deleted or otherwise modified during deletion. The user than can decide, if it
is a wise solution to delete the data. An example cascade deletion handler
looks like this:

```java
public class LabelTaskCascadeDeletion implements CascadeDeletion<LabelEntity> {

    @Override
    public int affectedByDeletion(EntityManager em, LabelEntity label) {
        return em.createQuery("select count(t) from Task t where :label in elements(t.labels)", Number.class)
                .setParameter("label", label)
                .getSingleResult()
                .intValue();
    }

    @Override
    public int delete(EntityManager em, LabelEntity label) {
        List<TaskEntity> tasks = em.createQuery("select t from Task t where :label in elements(t.labels)", TaskEntity.class)
                .setParameter("label", label)
                .getResultList();
        for (TaskEntity task : tasks) {
            task.labels.remove(label);
        }
        return tasks.size();
    }

    @Override
    public DeletionAction getAction() {
        return new DeletionAction("Tasks using the label", "will loose the label");
    }

}
```

## Contributions

This project is targetted towards small projects. It is not meant to be used in
large scale or otherwise critical projects. You can use the code unter the MIT
license as you wish, and you are free to contribute this back to the project to
share it with others.

Please fork the project and create a merge request for your contribution. Please
note that unit tests are strongly suggested for each merge request. If they are
not available, a merge request must not cause regressions in the existing
tests of the project.

The maintainer of this projects remains the right to reject the merge request
in case the contribution is not relevant to the project, or the contributor
does not follow the contribution rules.

### Reporting issues

This project uses the Gitlab issues (https://gitlab.com/appframework/appframework/-/issues)
to track requests. Please note that raising an issue does not imply any
guarantees on when or even if a request is completed. To speed up the
resolution of the issue, feel free to provide a merge request which adresses
it.