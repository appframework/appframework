package de.christophbrill.appframework.persistence.deletion;

public class UserRoleCascadeDeletion extends AbstractUserCreatedModifiedDeletion {

    @Override
    protected String getEntityName() {
        return "Role";
    }
}
