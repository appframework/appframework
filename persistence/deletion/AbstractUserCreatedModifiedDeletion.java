package de.christophbrill.appframework.persistence.deletion;

import de.christophbrill.appframework.persistence.model.UserEntity;
import de.christophbrill.appframework.ui.dto.DeletionAction;
import jakarta.persistence.EntityManager;

/**
 * Tasks to be performed when a user is being deleted
 */
public abstract class AbstractUserCreatedModifiedDeletion implements CascadeDeletion<UserEntity> {

    /**
     * The name of the entity being affected by the deletion
     */
    protected abstract String getEntityName();

    /**
     * Calculates the number of entities affected by the deletion, due to the user being the creator or modifier.
     */
    @Override
    public int affectedByDeletion(EntityManager em, UserEntity user) {
        return em
            .createQuery(
                "select count(a) from " + getEntityName() + " a where a.createdBy = :user or a.modifiedBy = :user",
                Number.class
            )
            .setParameter("user", user)
            .getSingleResult()
            .intValue();
    }

    /**
     * Performs the deletion. It unsets 'createdBy' and 'modifiedBy' fields for entities crated or modified by the user.
     */
    @Override
    public int delete(EntityManager em, UserEntity user) {
        int r1 = em
            .createQuery("update " + getEntityName() + " set createdBy = null where createdBy = :user")
            .setParameter("user", user)
            .executeUpdate();
        int r2 = em
            .createQuery("update " + getEntityName() + " set modifiedBy = null where modifiedBy = :user")
            .setParameter("user", user)
            .executeUpdate();
        return r1 + r2;
    }

    /**
     * Get a DeletionAction instance describing the actions performed by this instance.
     */
    @Override
    public DeletionAction getAction() {
        return new DeletionAction(
            getEntityName() + "s created by the user",
            "will unset creator and modifier of " + getEntityName()
        );
    }
}
