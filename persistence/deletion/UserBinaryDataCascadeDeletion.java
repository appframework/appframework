package de.christophbrill.appframework.persistence.deletion;

import de.christophbrill.appframework.persistence.model.UserEntity;
import de.christophbrill.appframework.ui.dto.DeletionAction;
import jakarta.persistence.EntityManager;

public class UserBinaryDataCascadeDeletion implements CascadeDeletion<UserEntity> {

    @Override
    public int affectedByDeletion(EntityManager em, UserEntity user) {
        return em
            .createQuery(
                "select count(b) from BinaryData b where b.createdBy = :user or b.modifiedBy = :user",
                Number.class
            )
            .setParameter("user", user)
            .getSingleResult()
            .intValue();
    }

    @Override
    public int delete(EntityManager em, UserEntity user) {
        return em
            .createQuery("delete from BinaryData where createdBy = :user or modifiedBy = :user")
            .setParameter("user", user)
            .executeUpdate();
    }

    @Override
    public DeletionAction getAction() {
        return new DeletionAction(
            "Binary data objects created by the user",
            "will unset creator and modifier of binary data objects"
        );
    }
}
