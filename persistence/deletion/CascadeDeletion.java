package de.christophbrill.appframework.persistence.deletion;

import de.christophbrill.appframework.ui.dto.DeletionAction;
import jakarta.persistence.EntityManager;

public interface CascadeDeletion<T> {
    int affectedByDeletion(EntityManager em, T t);

    int delete(EntityManager em, T t);

    DeletionAction getAction();
}
