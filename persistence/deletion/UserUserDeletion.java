package de.christophbrill.appframework.persistence.deletion;

public class UserUserDeletion extends AbstractUserCreatedModifiedDeletion {

    @Override
    protected String getEntityName() {
        return "User";
    }
}
