package de.christophbrill.appframework.persistence.deletion;

import de.christophbrill.appframework.persistence.model.RoleEntity;
import de.christophbrill.appframework.persistence.model.UserEntity;
import de.christophbrill.appframework.ui.dto.DeletionAction;
import jakarta.persistence.EntityManager;

public class RoleUserCascadeDeletion implements CascadeDeletion<RoleEntity> {

    @Override
    public int affectedByDeletion(EntityManager em, RoleEntity role) {
        return em
            .createQuery("select count(u) from User u where :role in elements(u.roles)", Number.class)
            .setParameter("role", role)
            .getSingleResult()
            .intValue();
    }

    @Override
    public int delete(EntityManager em, RoleEntity role) {
        var users = em
            .createQuery("select u from User u where :role in elements(u.roles)", UserEntity.class)
            .setParameter("role", role)
            .getResultList();
        for (UserEntity user : users) {
            user.roles.remove(role);
        }
        return users.size();
    }

    @Override
    public DeletionAction getAction() {
        return new DeletionAction("User in roles", "will remove user from roles");
    }
}
