package de.christophbrill.appframework.persistence.listeners;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.hibernate.annotations.ValueGenerationType;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.METHOD, ElementType.ANNOTATION_TYPE })
@ValueGenerationType(generatedBy = InsertCurrentUserValueGeneration.class)
public @interface InsertGeneratedCurrentUserValue {
}
