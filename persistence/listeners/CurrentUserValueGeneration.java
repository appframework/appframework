package de.christophbrill.appframework.persistence.listeners;

import de.christophbrill.appframework.persistence.model.UserEntity;
import io.quarkus.logging.Log;
import io.quarkus.security.identity.CurrentIdentityAssociation;
import jakarta.enterprise.inject.spi.CDI;
import jakarta.persistence.FlushModeType;
import java.io.Serial;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.generator.BeforeExecutionGenerator;
import org.hibernate.generator.EventType;

abstract class CurrentUserValueGeneration implements BeforeExecutionGenerator {

    @Serial
    private static final long serialVersionUID = -3228801294656777462L;

    public Object generate(
        SharedSessionContractImplementor session,
        Object owner,
        Object currentValue,
        EventType eventType
    ) {
        CurrentIdentityAssociation identityAssociation = CDI.current().select(CurrentIdentityAssociation.class).get();
        var identity = identityAssociation.getIdentity();
        if (!identity.isAnonymous()) {
            Log.tracev("Checking for login {0}", identity.getPrincipal().getName());
            currentValue = session
                .createQuery("from User where login = :login", UserEntity.class)
                .setParameter("login", identity.getPrincipal().getName())
                .setFlushMode(FlushModeType.COMMIT)
                .getSingleResult();
        }
        return currentValue;
    }
}
