package de.christophbrill.appframework.persistence.listeners;

import java.io.Serial;
import java.util.EnumSet;
import org.hibernate.generator.EventType;

public class UpdateCurrentUserValueGeneration extends CurrentUserValueGeneration {

    @Serial
    private static final long serialVersionUID = 4208010641442256292L;

    @Override
    public EnumSet<EventType> getEventTypes() {
        return EnumSet.of(EventType.INSERT, EventType.UPDATE);
    }
}
