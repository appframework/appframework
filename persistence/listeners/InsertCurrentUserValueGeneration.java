package de.christophbrill.appframework.persistence.listeners;

import java.io.Serial;
import java.util.EnumSet;
import org.hibernate.generator.EventType;

public class InsertCurrentUserValueGeneration extends CurrentUserValueGeneration {

    @Serial
    private static final long serialVersionUID = 1222217555943909865L;

    @Override
    public EnumSet<EventType> getEventTypes() {
        return EnumSet.of(EventType.INSERT);
    }
}
