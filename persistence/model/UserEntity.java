/*
 * Copyright 2013  Christoph Brill <christophbrill@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.christophbrill.appframework.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.io.Serial;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Christoph Brill &lt;christophbrill@gmail.com&gt;
 */
@Entity(name = "User")
@Table(name = "user_")
public class UserEntity extends DbObject {

    @Serial
    private static final long serialVersionUID = 5341258879773745459L;

    @Column(nullable = false)
    @NotNull
    @Size(min = 1, max = 255)
    public String name;

    @Column(nullable = false, length = 63)
    @NotNull
    @Size(min = 1, max = 63)
    public String login;

    @Column(nullable = false, length = 40)
    @NotNull
    @Size(min = 1, max = 40)
    public String password;

    @Column(nullable = false)
    @NotNull
    @Size(min = 1, max = 255)
    public String email;

    @ManyToMany
    @JoinTable(
        name = "user_role",
        joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id")
    )
    @OrderBy("name")
    @JsonIgnore
    public List<RoleEntity> roles = new ArrayList<>(0);

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "picture_id")
    @JsonIgnore
    public BinaryDataEntity picture;

    public String getPassword() {
        return password;
    }

    public static UserEntity findByName(String name){
        return find("name", name).firstResult();
    }
}
