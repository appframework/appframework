/*
 * Copyright 2013  Christoph Brill <christophbrill@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.christophbrill.appframework.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.christophbrill.appframework.persistence.listeners.InsertGeneratedCurrentUserValue;
import de.christophbrill.appframework.persistence.listeners.UpdateGeneratedCurrentUserValue;
import io.quarkus.hibernate.orm.panache.PanacheEntity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MappedSuperclass;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import org.hibernate.Hibernate;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

/**
 * @author Christoph Brill &lt;christophbrill@gmail.com&gt;
 */
@MappedSuperclass
public class DbObject extends PanacheEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = -3422412128666825823L;

    @CreationTimestamp
    @JsonIgnore
    public LocalDateTime created;

    @UpdateTimestamp
    @JsonIgnore
    public LocalDateTime modified;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "creator_id", nullable = false)
    @InsertGeneratedCurrentUserValue
    @JsonIgnore
    public UserEntity createdBy;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "modificator_id")
    @UpdateGeneratedCurrentUserValue
    @JsonIgnore
    public UserEntity modifiedBy;

    @Override
    public int hashCode() {
        if (id == null) {
            return super.hashCode();
        }
        return Objects.hash(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        DbObject dbObject = (DbObject) o;
        if (id == null) {
            return false;
        }
        return Objects.equals(id, dbObject.id);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(getClass().getSimpleName());
        if (id != null) {
            builder.append('@').append(id);
        } else {
            builder.append('#').append(hashCode());
        }
        return builder.toString();
    }
}
