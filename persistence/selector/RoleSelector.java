package de.christophbrill.appframework.persistence.selector;

import de.christophbrill.appframework.persistence.model.RoleEntity;
import de.christophbrill.appframework.persistence.model.RoleEntity_;
import de.christophbrill.appframework.persistence.model.UserEntity_;
import jakarta.annotation.Nonnull;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

public class RoleSelector extends AbstractResourceSelector<RoleEntity> {

    private String userLogin;
    private String search;

    public RoleSelector(EntityManager em) {
        super(em);
    }

    @Nonnull
    @Override
    protected Class<RoleEntity> getEntityClass() {
        return RoleEntity.class;
    }

    @Nonnull
    @Override
    protected List<Predicate> generatePredicateList(
        @Nonnull CriteriaBuilder builder,
        @Nonnull Root<RoleEntity> from,
        @Nonnull CriteriaQuery<?> criteriaQuery
    ) {
        List<Predicate> predicates = super.generatePredicateList(builder, from, criteriaQuery);

        if (StringUtils.isNotEmpty(userLogin)) {
            predicates.add(builder.equal(from.join(RoleEntity_.users).get(UserEntity_.login), userLogin));
        }

        if (StringUtils.isNotEmpty(search)) {
            String likePattern = '%' + search + '%';
            predicates.add(builder.or(builder.like(from.get(RoleEntity_.name), likePattern)));
        }

        return predicates;
    }

    public RoleSelector withUserLogin(String userLogin) {
        this.userLogin = userLogin;
        return this;
    }

    @Override
    public RoleSelector withSearch(String search) {
        this.search = search;
        return this;
    }
}
