/*
 * Copyright 2013-2015  Christoph Brill <christophbrill@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.christophbrill.appframework.persistence.selector;

import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;
import jakarta.persistence.EntityManager;
import jakarta.persistence.FlushModeType;
import jakarta.persistence.LockModeType;
import jakarta.persistence.NoResultException;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Order;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

/**
 * @author Christoph Brill &lt;christophbrill@gmail.com&gt;
 */
public abstract class AbstractSelector<T> {

    private LockModeType lockMode;
    private FlushModeType flushMode;

    private final EntityManager em;

    public AbstractSelector(EntityManager em) {
        this.em = em;
    }

    protected CriteriaBuilder createCriteriaBuilder() {
        return em.getCriteriaBuilder();
    }

    private TypedQuery<T> buildQuery() {
        CriteriaBuilder builder = createCriteriaBuilder();
        CriteriaQuery<T> cq = builder.createQuery(getEntityClass());
        Root<T> from = cq.from(getEntityClass());
        List<Predicate> predicates = generatePredicateList(builder, from, cq);
        cq.where(predicates.toArray(new Predicate[0]));
        cq.orderBy(generateOrderList(builder, from));
        cq.select(from);
        TypedQuery<T> query = em.createQuery(cq);
        if (lockMode != null) {
            query.setLockMode(lockMode);
        }
        if (flushMode != null) {
            query.setFlushMode(flushMode);
        }
        return query;
    }

    @Nonnull
    public List<T> findAll() {
        TypedQuery<T> q = buildQuery();
        if (limit != null) {
            q.setMaxResults(limit);
        }
        if (offset != null) {
            q.setFirstResult(offset);
        }
        q.setLockMode(LockModeType.NONE);
        return q.getResultList();
    }

    @Nullable
    public T find() {
        TypedQuery<T> q = buildQuery();
        if (limit != null) {
            q.setMaxResults(limit);
        }
        if (offset != null) {
            q.setFirstResult(offset);
        }
        try {
            return q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public Long count() {
        CriteriaBuilder builder = createCriteriaBuilder();
        CriteriaQuery<Long> cq = builder.createQuery(Long.class);
        Root<T> from = cq.from(getEntityClass());
        List<Predicate> predicates = generatePredicateList(builder, from, cq);
        cq.where(predicates.toArray(new Predicate[0]));
        cq.select(builder.count(from));
        TypedQuery<Long> q = em.createQuery(cq);
        return q.getSingleResult();
    }

    @Nonnull
    protected abstract Class<T> getEntityClass();

    @Nonnull
    protected abstract List<Predicate> generatePredicateList(
        @Nonnull CriteriaBuilder builder,
        @Nonnull Root<T> from,
        @Nonnull CriteriaQuery<?> criteriaQuery
    );

    @Nonnull
    protected List<Order> generateOrderList(@Nonnull CriteriaBuilder builder, @Nonnull Root<T> from) {
        if (CollectionUtils.isNotEmpty(sortColumns)) {
            List<Order> result = new ArrayList<>(sortColumns.size());
            for (Pair<String, Boolean> sortColumn : sortColumns) {
                Path<?> fromTable = from;
                String column = sortColumn.getLeft();

                // Check if ordering is done by a subquery
                String[] split = sortColumn.getLeft().split("\\.");
                if (split.length > 1) {
                    // Jump all the joins until one before the last
                    for (int i = 0; i < split.length - 1; i++) {
                        fromTable = fromTable.get(split[i]);
                    }
                    // The last is the column to sort by
                    column = split[split.length - 1];
                }
                if (!Boolean.FALSE.equals(sortColumn.getRight())) {
                    result.add(builder.asc(fromTable.get(column)));
                } else {
                    result.add(builder.desc(fromTable.get(column)));
                }
            }
            return result;
        }
        return getDefaultOrderList(builder, from);
    }

    protected List<Order> getDefaultOrderList(@Nonnull CriteriaBuilder builder, @Nonnull Root<T> from) {
        return Collections.emptyList();
    }

    private Integer offset;
    private Integer limit;
    protected List<Pair<String, Boolean>> sortColumns;

    public AbstractSelector<T> withOffset(Integer offset) {
        this.offset = offset;
        return this;
    }

    public AbstractSelector<T> withLimit(Integer limit) {
        this.limit = limit;
        return this;
    }

    public AbstractSelector<T> withSortColumn(String sortColumn, boolean ascending) {
        if (StringUtils.isEmpty(sortColumn)) {
            return this;
        }

        if (sortColumns == null) {
            sortColumns = new ArrayList<>();
        }
        sortColumns.add(new ImmutablePair<>(sortColumn, ascending));

        return this;
    }

    public AbstractSelector<T> withLockMode(LockModeType lockMode) {
        this.lockMode = lockMode;
        return this;
    }

    public AbstractSelector<T> withFlushMode(FlushModeType flushMode) {
        this.flushMode = flushMode;
        return this;
    }
}
