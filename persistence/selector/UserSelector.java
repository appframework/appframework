package de.christophbrill.appframework.persistence.selector;

import de.christophbrill.appframework.persistence.model.UserEntity;
import de.christophbrill.appframework.persistence.model.UserEntity_;
import jakarta.annotation.Nonnull;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

public class UserSelector extends AbstractResourceSelector<UserEntity> {

    private String login;
    private String search;

    public UserSelector(EntityManager em) {
        super(em);
    }

    @Nonnull
    @Override
    protected Class<UserEntity> getEntityClass() {
        return UserEntity.class;
    }

    @Nonnull
    @Override
    protected List<Predicate> generatePredicateList(
        @Nonnull CriteriaBuilder builder,
        @Nonnull Root<UserEntity> from,
        @Nonnull CriteriaQuery<?> criteriaQuery
    ) {
        List<Predicate> predicates = super.generatePredicateList(builder, from, criteriaQuery);

        if (StringUtils.isNotEmpty(login)) {
            predicates.add(builder.equal(from.get(UserEntity_.login), login));
        }

        if (StringUtils.isNotEmpty(search)) {
            String likePattern = '%' + search + '%';
            predicates.add(
                builder.or(
                    builder.like(from.get(UserEntity_.login), likePattern),
                    builder.like(from.get(UserEntity_.name), likePattern),
                    builder.like(from.get(UserEntity_.email), likePattern)
                )
            );
        }

        return predicates;
    }

    public UserSelector withLogin(String login) {
        this.login = login;
        return this;
    }

    @Override
    public UserSelector withSearch(String search) {
        this.search = search;
        return this;
    }
}
