package de.christophbrill.appframework.persistence.selector;

import de.christophbrill.appframework.persistence.model.DbObject;
import de.christophbrill.appframework.persistence.model.DbObject_;
import jakarta.annotation.Nonnull;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;

public abstract class AbstractResourceSelector<T extends DbObject> extends AbstractSelector<T> {

    private Collection<Long> ids;

    public AbstractResourceSelector(EntityManager em) {
        super(em);
    }

    @Override
    @Nonnull
    protected List<Predicate> generatePredicateList(
        @Nonnull CriteriaBuilder builder,
        @Nonnull Root<T> from,
        @Nonnull CriteriaQuery<?> criteriaQuery
    ) {
        List<Predicate> predicates = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(ids)) {
            predicates.add(from.get(DbObject_.id).in(ids));
        }

        return predicates;
    }

    public AbstractResourceSelector<T> withId(Long id) {
        ids = Collections.singleton(id);
        return this;
    }

    public AbstractResourceSelector<T> withIds(Collection<Long> ids) {
        this.ids = ids;
        return this;
    }

    public abstract AbstractResourceSelector<T> withSearch(String search);
}
